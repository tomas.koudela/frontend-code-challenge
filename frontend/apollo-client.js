import { ApolloClient, InMemoryCache } from "@apollo/client";
import { offsetLimitPagination, concatPagination } from "@apollo/client/utilities";

const client = new ApolloClient({
  uri: "http://localhost:4000/graphql",
  cache: new InMemoryCache(),
});

export default client;