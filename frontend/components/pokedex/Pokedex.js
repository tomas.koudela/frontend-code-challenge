import { useState } from 'react';
import Container from '@material-ui/core/Container';
import PokedexHeader from '../pokedexHeader/PokedexHeader';
import PokedexContent from './PokedexContent';
import styles from './pokedex.module.scss';

function Pokedex({type, content}) {
  const [variables, setVariables] = useState(
    {
      limit: 9,
      offset: 0,
      search: '',
      filter: { type: '', isFavorite: false},
      page: 1,
    }
  );
  const [displayAs, setDisplayAs] = useState('grid');

  const getNewVariable = (newLimit, newOffset, newSearch, newPage, filterType, filterFavorite) => {
    const newVar = {
      limit: newLimit,
      offset: newOffset,
      search: newSearch,
      filter: { type: filterType, isFavorite: filterFavorite },
      page: newPage,
    }
    setVariables(newVar);
  };
  
  return (
    <Container className = {styles.container__main}>
      <div className={styles.pokedex_header_card} elevation="5">
        {type === 'home' && <PokedexHeader 
          setDisplayAs={setDisplayAs} 
          onChange={
            (
              limit, 
              offset, 
              string, 
              newPage, 
              filterType, 
              filterFavorite
            ) => 
              getNewVariable(
                limit, 
                offset, 
                string, 
                newPage, 
                filterType, 
                filterFavorite
              )}
        />
        }
      </div>
      <div className={styles.pokedex} elevation="5">
        <PokedexContent
          displayAs={displayAs}
          setDisplayAs={setDisplayAs}
          contentType={type}
          pokedexContent={content}
          variables={variables}
          getNewVariable={getNewVariable}
        />
      </div>
    </Container>
  )
}

export default Pokedex;

