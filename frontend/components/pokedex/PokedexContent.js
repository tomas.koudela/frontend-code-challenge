import { useQuery, useMutation, gql } from "@apollo/client";
import { Pagination } from '@material-ui/lab';

import styles from './pokedex.module.scss';
import PokedexContentHome from './PokedexContentHome';
import PokedexContentDetail from './PokedexContentDetail';
import grapghQlCalls from '../../gql/Gql';

const QUERY = gql`query Pokemons($limit: Int!, $offset: Int!, $search: String, $filter: PokemonFilterInput) {
  pokemons(query: { limit: $limit, offset: $offset, search: $search, filter: $filter }) {
    limit
    offset
    count
    edges {
      id
      name
      classification
      maxHP
      attacks {fast {name, type, damage}, special {name, type, damage}}
      height {maximum}
      weight {maximum}
      types
      image
      sound
      isFavorite
    }
  }
}`;

function PokedexContent({ contentType, pokedexContent, getOffset, displayAs, variables, getNewVariable }) {
  const { data, loading, error } = useQuery(QUERY, {
    variables,
    fetchPolicy: 'no-cache'
  });

  const [pokemonFavorite] = useMutation(grapghQlCalls.FAVORITE_POKEMON);
  const [pokemonUnfavorite] = useMutation(grapghQlCalls.UNFAVORITE_POKEMON);
  
  const getPageAmount = (count, limit) => {
    return Math.ceil(count / limit);
  };

  const handlePageClick = (e, value, limit) => {
    getNewVariable(9, (value - 1) * limit, variables.search, value, variables.filter.type, variables.filter.isFavorite);
  };

  if (loading) {
    return <h2>Loading...</h2>;
  } else if (error) {
    console.error(error);
    return null;
  } else {
    if (contentType === 'home') {
      return (
        <div className={styles.pokedex__content_wrapper}>
          <div className={styles.pokedex__content_top}>
            <Pagination 
              count={getPageAmount(data.pokemons.count, data.pokemons.limit)} 
              page={variables.page} 
              onChange={(e, value) => handlePageClick(e, value, data.pokemons.limit)} 
            />
          </div>
          <PokedexContentHome 
            displayAs={displayAs}
            count={data.pokemons.count} 
            limit={data.pokemons.limit}
            pokemonFavorite={pokemonFavorite} 
            pokemonUnfavorite={pokemonUnfavorite} 
            content={data.pokemons.edges}
            getOffset={getOffset}
          />
        </div>
      )
    } else if (contentType === 'detail') {
      return (
        <PokedexContentDetail content={pokedexContent} />
      )
    }
  }
}

export default PokedexContent;


