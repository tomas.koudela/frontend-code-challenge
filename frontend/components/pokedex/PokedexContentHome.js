import classes from 'react-style-classes';
import PokemonCard from '../pokemonCard/PokemonCard';
import styles from './pokedex.module.scss';

function PokedexContentHome({ content, pokemonFavorite, pokemonUnfavorite, displayAs }) {
  return (
    <section 
      className={
        classes(
          styles.pokedex__content,
          displayAs === 'grid' && styles.grid,
          displayAs === 'list' && styles.list
        )
      }
    >
      {content.map(pokemon => {
        return (
          <PokemonCard displayAs={displayAs} pokemon={pokemon} pokemonUnfavorite={pokemonUnfavorite} pokemonFavorite={pokemonFavorite} />
        )
      })}
    </section>
  )
}

export default PokedexContentHome

