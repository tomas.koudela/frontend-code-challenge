import Link from 'next/link';

import { useAudio } from '../../customHooks/useAudio';
import Typography from '@material-ui/core/Typography';
import { IconButton } from '@material-ui/core';
import VolumeUpSharpIcon from '@material-ui/icons/VolumeUpSharp';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import styles from './pokedex.module.scss';

function PokedexContentDetail({ content }) {
  const [playing, toggle] = useAudio(content.sound);

  const getBackground = (type) => {
    if (type === 'Grass') {
      return '#48D0B0';
    } else if (type === 'Fire') {
      return '#FB6B6C';
    }
    else if (type === 'Water') {
      return '#76BDFE';
    } else {
      return '#FFD86F';
    }
  };

  return (
    <div className={styles.detail__wrapper}>
      <section className={styles.detail__header_top}>
        <Link href="/">
          <IconButton>
            <ChevronLeftIcon />
          </IconButton>
        </Link>
        <Typography variant="h4">
          <span className={styles.about__data}>{content.name}</span>
        </Typography>
      </section>
      <section className={styles.detail__basic_info}>
        <div className={styles.image__wrapper}>
          <img src={content.image} />
        </div>
        <div style={{ background: `${getBackground(content.types[0])}` }} className={styles.detail__basic_card}>
          <Typography className={styles.about__text}>
            Species: <span className={styles.about__data}>{content.classification}</span>
          </Typography>
          <Typography className={styles.about__text}>
            Height: <span className={styles.about__data}>{content.height.maximum}</span>
          </Typography>
          <Typography className={styles.about__text}>
            weight: <span className={styles.about__data}>{content.weight.maximum}</span>
          </Typography>
          <Typography className={styles.about__text}>
            Sound: 
            <IconButton onClick={toggle} className={styles.detail__basic_sound}>
              <VolumeUpSharpIcon color="white" />
            </IconButton>
          </Typography>
        </div>
      </section>
      <section className={styles.detail__charts}>
        <div className={styles.detail__charts_header}>
          <Typography className={styles.detail__charts_title}>
            MAX CP
            </Typography>
          <Typography className={styles.detail__charts_title}>
            {content.maxCP}
          </Typography>
        </div>

        <div color="primary.main" className={styles.detail__chart_CP} />
        <div className={styles.detail__charts_header}>
          <Typography className={styles.detail__charts_title}>
            MAX HP
            </Typography>
          <Typography className={styles.detail__charts_title}>
            {content.maxHP}
          </Typography>
        </div>
        <div className={styles.detail__chart_HP} />
      </section>
      <section style={{ background: `${getBackground(content.types[0])}` }} className={styles.detail__other_info}>
        <Typography className={styles.about__text}>
          Fast Attacks: {content.attacks.fast.map(atack => {
          return (
            <span className={styles.about__data}>{atack.name},</span>
          )
        })}
        </Typography>
        <Typography className={styles.about__text}>
          Spacial Attacks: {content.attacks.special.map(atack => {
          return (
            <span className={styles.about__data}>{atack.name},</span>
          )
        })}
        </Typography>
        <Typography className={styles.about__text}>
          Weaknesses: {content.weaknesses.map(weakness => {
          return (
            <span className={styles.about__data}>{weakness},</span>
          )
        })}
        </Typography>
        {content.evolutionRequirements && <Typography className={styles.about__text}>
          Evolution requirements:
            <span className={styles.about__data}>
            {` ${content.evolutionRequirements.name}: ${content.evolutionRequirements.amount}`}
          </span>
        </Typography>}
      </section>
      <section className={styles.detail__evolutions}>
        <div className={styles.detail__evolutions_header}>
          <Typography className={styles.detail__evolutions_header_text}>
            {content.evolutions.length > 0 ? 'Evolutions:' : 'This pokémon is fully evolved'}
          </Typography>
        </div>
        <div className={styles.detail__evolutions_content}>
          {content.evolutions.map(evolution => {
            return (
              <Link href={`${evolution.name}`}>
                <div className={styles.detail__evolutions_card}>
                  <Typography className={styles.detail__evolutions_card_text}>
                    {evolution.name}
                  </Typography>
                  <img src={evolution.image} />
                </div>
              </Link>
            )
          })}
        </div>
      </section>
    </div>
  )
}

export default PokedexContentDetail

