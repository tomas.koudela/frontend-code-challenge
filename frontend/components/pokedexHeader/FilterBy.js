import { useState } from 'react';
import React from 'react';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { IconButton } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { useQuery, gql } from "@apollo/client";
import AppsIcon from '@material-ui/icons/Apps';
import ListIcon from '@material-ui/icons/List';
import Tooltip from '@material-ui/core/Tooltip';
import Fade from '@material-ui/core/Fade';

import grapghQlCalls from '../../gql/Gql';
import styles from './PokedexHeader.module.scss';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function content({ onChange, setDisplayAs}) {
  const { data, loading, error } = useQuery(grapghQlCalls.QUERYTYPE);
  const [pokemonType, setPokemonType] = useState();
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChange = (event) => {
    setPokemonType(event.target.value);
    onChange(9, 0, '', 1, event.target.value, false);
  };

  const handleFilterFavorite = (isFavorite) => {
    if (isFavorite) 
      onChange(9, 0, '', 1, '', isFavorite);
    else {
      onChange(9, 0, '', 1, '', false);
    }
  };

  if (loading) {
    return <h2>Loading...</h2>;
  }

  if (error) {
    console.error(error);
    return null;
  }

  return (
    <section className={styles.advanced__wrapper}>
      <Tooltip
        TransitionComponent={Fade}
        TransitionProps={{ timeout: 600 }}
        title="Display as grid"
        placement="top"
      >
        <IconButton color="secondary" onClick={() => setDisplayAs('grid')}>
          <AppsIcon />
        </IconButton>
      </Tooltip>
      <Tooltip
        TransitionComponent={Fade}
        TransitionProps={{ timeout: 600 }}
        title="Display as list"
        placement="top"
      >
        <IconButton color="secondary" onClick={() => setDisplayAs('list')}>
          <ListIcon />
        </IconButton>
      </Tooltip>
      <Button className={styles.advanced} variant="contained" color="secondary" onClick={handleClickOpen}>
        Advanced search
      </Button>
      <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle>
          Advanced search
          <IconButton className={styles.dialog__closeButton} onClick={() => handleClose()}>
            <CloseIcon />
          </IconButton>
        </DialogTitle>
        <DialogContent className={styles.dialog__content}>
          <FormControl variant="outlined" className={styles.dialog__content_dropdown}>
            <InputLabel htmlFor="outlined-age-native-simple">Type</InputLabel>
            <Select
              native
              value={pokemonType}
              onChange={handleChange}
              label="Type"
            >
              {data.pokemonTypes.map(type => {
                return (
                  <option value={type}>{type}</option>
                )
              })}
            </Select>
          </FormControl>
          <div className={styles.dialog__content_buttons}>
            <Button onClick={() => handleFilterFavorite(false)} variant="contained" color="secondary">
              All
            </Button>
            <Button onClick={() => handleFilterFavorite(true)} variant="contained" color="secondary">
              Favorite
            </Button>
          </div>
        </DialogContent>
      </Dialog>
    </section>
  )
}

export default content

