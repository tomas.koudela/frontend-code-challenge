import React from 'react';

import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';

import FilterByType from './FilterBy';
import logo from '../../public/logo.png';
import styles from './PokedexHeader.module.scss';

function content({ onChange, setDisplayAs,}) {
  const handleSearch = (e) => {
    onChange(9, 0, e.target.value, 1, '', false);
  };

  return (
    <section className={styles.pokedex__header}>
      <div className={styles.logo__section}>
        <img src={logo} alt="pokedex logo" />
        <Typography variant="h3" >Pokedex</Typography>
      </div>
      <Typography className={styles.section__description}>
        Feel free to search your pokemon to see his detail and to pick your champion. Good Luck!
      </Typography>
      <div className={styles.search__section}>
        <section className={styles.searchBar__wrapper}>
          <TextField
            className={styles.searchBar}
            label="Search your pokemon"
            variant="filled"
            fullWidth
            onChange={(e) => handleSearch(e)}
          />
        </section>
        
        <FilterByType setDisplayAs={setDisplayAs} onChange={onChange} />
      </div>
    </section>
  )
}

export default content

