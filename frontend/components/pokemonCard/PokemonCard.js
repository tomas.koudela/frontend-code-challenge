import { useState } from 'react';
import classes from 'react-style-classes';
import Link from 'next/link';

import Button from '@material-ui/core/Button';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import FavoriteIcon from '@material-ui/icons/Favorite';

import Description from './Description';
import styles from './PokemonCard.module.scss';

function PokemonCard({ pokemon, pokemonFavorite, pokemonUnfavorite, displayAs }) {
  const [display, setDisplay] = useState('About');
  const [isFavorite, setIsFavorite] = useState(pokemon.isFavorite);

  const handleFavorite = (favorite, pokemonId) => {
    if (favorite === false) {
      pokemonUnfavorite({ variables: { id: pokemonId } });
      setIsFavorite(false);
    } else {
      pokemonFavorite({ variables: { id: pokemonId } });
      setIsFavorite(true);
    }
  };

  const getBackground = (type) => {
    if (type === 'Grass') {
      return '#48D0B0';
    } else if (type === 'Fire') {
      return '#FB6B6C';
    }
    else if (type === 'Water') {
      return '#76BDFE';
    } else {
      return '#FFD86F';
    }
  };
    return (
      <div 
        className={
          classes(
            styles.pokemonCard,
            displayAs === 'grid' && styles.grid,
            displayAs === 'list' && styles.list
            )
          }
        >
        <section className={
          classes(
            styles.card__header,
            displayAs === 'grid' && styles.card__header_grid,
            displayAs === 'list' && styles.card__header_list
            )
          }
        >
          <img className={
              classes(
                displayAs === 'grid' && styles.card__image_grid,
                displayAs === 'list' && styles.card__image_list,
              )
            } 
            src={pokemon.image} 
            alt="pokemon image" 
          />
          {isFavorite && <FavoriteIcon color="error" onClick={() => handleFavorite(false, pokemon.id)}/>}
          {!isFavorite && <FavoriteBorderIcon color="error" onClick={() => handleFavorite(true, pokemon.id)}/>}
        </section>
        <section
          className={
            classes(
              styles.card__content,
              displayAs === 'grid' && styles.card__content_grid,
              displayAs === 'list' && styles.card__content_list
              )
            }
          style={{ background: `${getBackground(pokemon.types[0])}` }}
        >
          <div className={
            classes(
              styles.card__content_nav,
              displayAs === 'grid' && styles.card__content_nav_grid,
              displayAs === 'list' && styles.card__content_nav_list
              )
            }
          >
            <Button onClick={() => setDisplay('About')} className={styles.card__content_nav_button} variant="contained" color="primary">
              About
          </Button>
            <Button onClick={() => setDisplay('Stats')} className={styles.card__content_nav_button} variant="contained" color="primary">
              Stats
          </Button>
            <Link href={`${pokemon.name}`}>
              <Button className={styles.card__content_nav_button} variant="contained" color="primary">
                Detail
            </Button>
            </Link>
          </div>
          <Description pokemon={pokemon} display={display} />
        </section>
      </div>
    )
}

export default PokemonCard

