import styles from './PokemonCard.module.scss';
import { Typography } from '@material-ui/core';

function PokemonCard({ display, pokemon }) {

  if (display === 'About') {
    return (
      <div>
        <Typography className={styles.about__text}>
          Name: <span className={styles.about__data}>{pokemon.name}</span>
        </Typography>
        <Typography className={styles.about__text}>
          Species: <span className={styles.about__data}>{pokemon.classification}</span>
        </Typography>
        <Typography className={styles.about__text}>
          Height: <span className={styles.about__data}>{pokemon.height.maximum}</span>
        </Typography>
        <Typography className={styles.about__text}>
          weight: <span className={styles.about__data}>{pokemon.weight.maximum}</span>
        </Typography>
      </div>
    )
  } else if (display === 'Stats') {
    return (
      <div>
        <Typography className={styles.about__text}>
          HP: <span className={styles.about__data}>{pokemon.maxHP}</span>
        </Typography>
        <Typography className={styles.about__text}>
          Fast Attacks: {pokemon.attacks.fast.map(atack => {
            return (
              <span className={styles.about__data}>{atack.name}</span>
            )
          })}
        </Typography>
        <Typography className={styles.about__text}>
          Spacial Attacks: {pokemon.attacks.special.map(atack => {
          return (
            <span className={styles.about__data}>{atack.name}</span>
          )
        })}
        </Typography>
      </div>
    )
  }
}

export default PokemonCard;

