import { useRouter } from 'next/router';

import { useQuery, gql } from "@apollo/client";

import grapghQlCalls from '../gql/Gql';
import Pokedex from '../components/pokedex/Pokedex'; 
import bodybg from '../public/bodybg.png';
import styles from '../styles/index.module.scss';

const QUERY = gql`query PokemonByName($name: String!) {
  pokemonByName( name: $name ) {
    image
    types
    name
    sound
    classification
    height {maximum}
    weight {maximum}
    maxCP
    maxHP
    attacks {fast {name, type, damage}, special {name, type, damage}}
    evolutionRequirements {name amount}
    weaknesses
    evolutions {name, image}
  }
}`;

export default function Name() {
  const router = useRouter();
  console.log(router.query.name)
  const { data, loading, error } = useQuery(QUERY, {
    variables : {
      name: router.query.name
    }
  });
  
  if (loading) {
    return <h2>Loading...</h2>;
  } else if (error) {
    console.error(error);
    return null;
  } else {
    return (
      <div className={styles.main__wrapper}>
        <Pokedex type='detail' content={data.pokemonByName} />
        <img src={bodybg} alt="background img" className={styles.main__wrapper_background} />
      </div>
    )
  }
}

export async function getServerSideProps(context) {
  return {
    props: {},
  };
}