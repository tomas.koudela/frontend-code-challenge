import Pokedex from '../components/pokedex/Pokedex';
import bodybg from '../public/bodybg.png';
import styles from '../styles/index.module.scss';

export default function Home() {
  return (
    <div className={styles.main__wrapper}>    
      <Pokedex type="home" />
      <img src={bodybg} alt="background img" className={styles.main__wrapper_background}/>
    </div>
  )
}

export async function getServerSideProps(context) {
  return {
    props: {},
  };
}