import { gql } from "@apollo/client";
 
const grapghQlCalls = {

  QUERYTYPE: gql`query {
    pokemonTypes
  }`,

  FAVORITE_POKEMON: gql`mutation FavoritePokemon($id: ID!) {
      favoritePokemon(id: $id) {
        id
        isFavorite
      }
    }
  `,

  UNFAVORITE_POKEMON: gql`
  mutation UnFavoritePokemon($id: ID!) {
      unFavoritePokemon(id: $id) {
        id
        isFavorite
      }
    }
  `,
}

export default grapghQlCalls;